<?php

require_once __DIR__ . '/ezSQLHelper.php';

/**
 * Class dbHelper
 */
class dbHelper extends ezSQLHelper
{
    /**
     * Register new user.
     *
     * @param string $username
     * @param string $password
     * @param string $firstname
     * @param string $lastname
     * @return bool|int|void
     */
    public function registerUser($username, $password, $firstname, $lastname)
    {
        $password = $this->createMd5($password);

        return $this->db->query("INSERT INTO `users` (username, password, firstname, lastname) VALUES ('{$username}', '{$password}', '{$firstname}', '{$lastname}') ");
    }

    /**
     * Validate user.
     *
     * @return bool|int|void
     */
    public function loginUser($username, $password)
    {
        $password = $this->createMd5($password);

        return $this->db->query("SELECT id from `users` WHERE `username` = '{$username}' AND `password` = '{$password}'");
    }

    /**
     * Create md5 password
     *
     * @param string $str
     * @return string
     */
    public function createMd5($str)
    {
        return md5($str);
    }
}