<?php

require_once __DIR__ . '/server/lib/soap/nusoap.php';

/**
 * Class Client
 *
 * Soap client class for managing the client object connecting to SOAP server.
 */
class Client
{
    /**
     * @var nusoap_client
     */
    protected $client;

    /**
     * Client constructor.
     *
     * @param string $serverUrl
     */
    public function __construct($serverUrl = '')
    {
        $this->client = new nusoap_client($serverUrl);
    }

    /**
     * @param string $username
     * @param string $password
     * @return string
     */
    public function loginUser($username, $password)
    {
        $this->client->call('login', [
            'username' => $username,
            'password' => $password
        ]);

        return $this->getResponse();
    }

    /**
     * Send request to soap server and register new user.
     *
     * @param string $username
     * @param string $password
     * @param string $firstname
     * @param string $lastname
     * @return string
     */
    public function registerUser($username, $password, $firstname, $lastname)
    {
        $this->client->call('register', [
            'username' => $username,
            'password' => $password,
            'firstname' => $firstname,
            'lastname' => $lastname
        ]);

        return $this->getResponse();
    }

    /**
     * Get last request.
     *
     * @return string
     */
    public function getRequest()
    {
        return $this->client->request;
    }

    /**
     * Get last request response.
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->client->response;
    }

    /**
     * Get error.
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->client->getError();
    }
}

/* creating object of the SOAP client to call the web service */

$client = new Client('YOUR_URL/server/server.php');

/* Calling the user register web service */ 

//$response = $client->registerUser('admin', 'admin@123', 'admin', 'user');

/* Calling the user login web service */ 

// $response = $client->loginUser('admin', 'admin@123');

/* Display the received response from server */ 
// echo htmlspecialchars($response, ENT_QUOTES);