## SOAP Server / Client Usage Test

Please follow below steps for the installation:

1. Open `client.php` and replace `YOUR_URL` to your application URL. (`Line no. 96`)
2. Open `server/.env` file, and chagne your database connection details there.
3. Directory `Database` contains the dump file for the database table.