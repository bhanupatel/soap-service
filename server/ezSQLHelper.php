<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/ezSQL/shared/ez_sql_core.php';
require_once __DIR__ . '/lib/ezSQL/pdo/ez_sql_pdo.php';

/**
 * Class ezSQLHelper
 */
class ezSQLHelper
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $dbname;

    /**
     * @var string
     */
    protected $db_user;

    /**
     * @var string
     */
    protected $db_password;

    /**
     * @var ezSQL_pdo
     */
    protected $db;

    /**
     * ezSQLHelper constructor.
     */
    public function __construct()
    {
        $this->loadEnvironment();
        $this->setDefaultConfig();
        $this->initEzSqlInstance();
    }

    /**
     * Load environment variables from env file
     */
    private function loadEnvironment()
    {
        $dotenv = new Dotenv\Dotenv(__DIR__);
        $dotenv->load();
    }

    /**
     * Load default config for database connection from environment variables
     */
    private function setDefaultConfig()
    {
        $this->setHost(getenv('DB_HOST'));
        $this->setDbname(getenv('DB_DATABASE'));
        $this->setDbUser(getenv('DB_USERNAME'));
        $this->setDbPassword(getenv('DB_PASSWORD'));
    }

    /**
     * Init ezSQL_pdo instance
     */
    public function initEzSqlInstance()
    {
        $this->db = new ezSQL_pdo("mysql:host={$this->host};dbname={$this->dbname}", $this->db_user, $this->db_password);
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @param string $dbname
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }

    /**
     * @param string $db_user
     */
    public function setDbUser($db_user)
    {
        $this->db_user = $db_user;
    }

    /**
     * @param string $db_password
     */
    public function setDbPassword($db_password)
    {
        $this->db_password = $db_password;
    }
}