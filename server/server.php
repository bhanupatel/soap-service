<?php

require_once __DIR__ . '/lib/soap/nusoap.php';
require_once __DIR__ . '/dbHelper.php';

$server = new nusoap_server;

// Register functions that works on server
$server->register('register');
$server->register('login');

/**
 * Register new user.
 *
 * @param string $username
 * @param string $password
 * @param string $firstname
 * @param string $lastname
 * @return array
 */
function register($username, $password, $firstname, $lastname)
{
    // Create Code
    $userId = (new dbHelper())->registerUser($username, $password, $firstname, $lastname);

    return [
    	'status' => 'OK',
    	'id' => $userId
    ];
}

/**
 * Check user login credential.
 *
 * @param string $username
 * @param string $password
 * @return array
 */
function login($username, $password)
{
    // Get user id if user login credential is valid
    $userId = (new dbHelper())->loginUser($username, $password);

    return [
        'status' => 'OK',
        'id' => ($userId > 0 ? $userId : false)
    ];
}

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
exit();